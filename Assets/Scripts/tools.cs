﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class tools : MonoBehaviour {

    public GameObject[] canvasDesactivables;
	public GameObject[] canvasTodos;
	private List<int> pilaAtras = new List<int> ();

	void Start(){
		agregarPilaAtras (5);
	}

	public void agregarPilaAtras(int a){
		if (pilaAtras.Count > 0) {
			if (a != pilaAtras [pilaAtras.Count - 1]) {
				pilaAtras.Add (a);
			}
		}else
			pilaAtras.Add (a);
	}

	public void restarPila(){
		if (pilaAtras.Count == 1)
			Application.Quit();
		else {
			if (pilaAtras.Count > 1) {
				canvasTodos [pilaAtras [pilaAtras.Count - 1]].SetActive (false);
				canvasTodos [pilaAtras [pilaAtras.Count - 2]].SetActive (true);
				pilaAtras.RemoveAt (pilaAtras.Count - 1);
			}
		}
	}

    void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)){
			restarPila ();
		}
    }

    public void destruirObjeto(GameObject a) {
        Destroy(a);        
    }

    public void entrar(GameObject entrar) {
        foreach (GameObject a in canvasDesactivables) {
            a.SetActive(false);
        }
        entrar.SetActive(true);
    }

    public void mostrarObjectos(GameObject[] a) {
		foreach(GameObject b in a)
			b.SetActive(true);
    }

    public void mostrarOcultar(GameObject a) {
        if (a.activeSelf) a.SetActive(false);
        else a.SetActive(true);
    }

    public void abrirURL(string a) {
        Application.OpenURL(a);
    }
}
