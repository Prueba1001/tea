﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Facebook.Unity;

public class LoginFacebookAlex : MonoBehaviour {

	////public GameObject DialogLoggedIn;
	////public GameObject DialogLoggedOut;
	public GameObject DialogUsername;
	public GameObject DialogProfilePic;

	public GameObject canvasLogo;

	void Awake() {
		FB.Init(SetInit, OnHideUnity);
	}

	void SetInit() {

		if (FB.IsLoggedIn) {
			Debug.Log("FB is logged in");
		} else {
			Debug.Log("FB is not logged in");
		}

		DealWithFBMenus(FB.IsLoggedIn);

	}

	void OnHideUnity(bool isGameShown) {

		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}

	}

	public void FBlogin() {

		List<string> permissions = new List<string>();
		permissions.Add("public_profile");
		permissions.Add("email");
		FB.LogInWithReadPermissions(permissions, AuthCallBack);
	}

	void AuthCallBack(IResult result) {

		if (result.Error != null) {
			Debug.Log(result.Error);
		} else {
			if (FB.IsLoggedIn) {
				Debug.Log("FB is logged in");
			} else {
				Debug.Log("FB is not logged in");
			}

			DealWithFBMenus(FB.IsLoggedIn);
		}

	}

	void DealWithFBMenus(bool isLoggedIn) {

		if (isLoggedIn) {
			//DialogLoggedIn.SetActive(true);
			//DialogLoggedOut.SetActive(false);

			FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
			FB.API("/me?fields=email", HttpMethod.GET, DisplayEmail);
			FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
			canvasLogo.SetActive (false);
		} else {
			//DialogLoggedIn.SetActive(false);
			//DialogLoggedOut.SetActive(true);
		}

	}

	void DisplayUsername(IResult result) {

		Text UserName = DialogUsername.GetComponent<Text>();

		if (result.Error == null) {

			UserName.text = "Hola, " + result.ResultDictionary["first_name"];

		} else {
			Debug.Log(result.Error);
		}

	}

	void DisplayEmail(IResult result) {

		Text UserName = DialogUsername.GetComponent<Text>();

		if (result.Error == null) {

			UserName.text = UserName.text + "  " + result.ResultDictionary["email"];

		} else {
			UserName.text = UserName.text + " asdsafasdf ";
		}

	}

	void DisplayProfilePic(IGraphResult result) {

		if (result.Texture != null) {

			RawImage ProfilePic = DialogProfilePic.GetComponent<RawImage>();
            ProfilePic.texture = result.Texture;
			//ProfilePic.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 251, 251), new Vector2());

		}

	}
}
