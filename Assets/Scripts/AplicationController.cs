﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AplicationController : MonoBehaviour {

    public int pagina=0;
    public GameObject principal;
    private string URL = "http://alexander.gzpot.com/pruebasPHP/displayTea.php";
    public string[,] datosProductos= new string[8,7];


    public void ponerProductos() {
        for (int i = 0; i < 4; i++)
        {
            GameObject.Find("Titulo" + (i + 1)).GetComponent<Text>().text = datosProductos[i + pagina*4,0];
            GameObject.Find("Descripcion" + (i + 1)).GetComponent<Text>().text = datosProductos[i + pagina*4,1];
        }
    }

    public void ponerDetalle(int a) {
        GameObject.Find("TituloDetalle").GetComponent<Text>().text=datosProductos[a+pagina*4,0];
        GameObject.Find("DescripcionLarga").GetComponent<Text>().text = datosProductos[a + pagina * 4,2];
        GameObject.Find("Precio").GetComponent<Text>().text = datosProductos[a + pagina * 4,4];
    }

    public void cambiarPaginacion(int mas) {
        pagina = pagina + mas;
        if (pagina + mas > (8 / 4)) {
            ;
        }
    }    

    IEnumerator ObtenerDatos()
    {
        WWW hs_get = new WWW(URL);
        yield return hs_get;
        if (hs_get.error != null) print("There was an error getting the high score: " + hs_get.error);
        else { 
            string[] temp1;
            string[] temp2;
            temp1 = hs_get.text.Split('%');
            for (int i = 0; i < 8; i++)
            {
                temp2 = temp1[i].Split('#');
                //foreach (string a in temp2) print(a + "\n");
                for (int j = 0; j < 6; j++)
                {
                    datosProductos[i,j] = temp2[j];
                }
            }
        }
        ponerProductos();
        StartCoroutine(cargarImagenes());
    }

    IEnumerator cargarImagenes()
    {
        for (int i = 0; i < 4; i++)
        {
            WWW www = new WWW(datosProductos[i+pagina*4,3]);
            yield return www;
            GameObject.Find("Slot"+(i+1)).GetComponent<RawImage>().texture = www.texture;
        }
    }

    public void cargarImagenPublic(int a) {
        StartCoroutine(cargarImagen(a));
    }

    IEnumerator cargarImagen(int a)
    {
        GameObject.Find("FotoDetalle").GetComponent<RawImage>().texture = null;
        WWW www = new WWW(datosProductos[a + pagina * 4, 3]);
        yield return www;
        GameObject.Find("FotoDetalle").GetComponent<RawImage>().texture = www.texture;
    }
}
