﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CargarDatos : MonoBehaviour {

	public RectTransform content;
	public GameObject slots;
	public GameObject[] slot;

	public GameObject botonLogin;
	public GameObject botonOmitir;
	public ComprobarInternet CI;
	public tools T;

	public GameObject canvasDetalle;
	public RawImage fotoDetalle;
	public Text nombreDetalle;
	public Text descripcionDetalle;
	public Text precioDetalle;
	public Button botonDetalle;

	private Texture[] imagenesProductos;
	private string[,] datosProductos;
	private string urlProducto = "http://alexander.gzpot.com/pruebasPHP/producto.php?";

	public void iniciarCarga(){
		StartCoroutine (cargar());
	}

	IEnumerator cargar(){
		WWW obtener = new WWW(urlProducto);
		yield return obtener;
		if (obtener.error != null) print("There was an error obteniendo los datos " + obtener.error);
		else { 
			string[] temp1;
			string[] temp2;
			temp1 = obtener.text.Split('|');
			datosProductos=new string[temp1.Length-1,5];
			for (int i = 0; i < temp1.Length-1; i++)
			{
				temp2 = temp1[i].Split('#');
				for (int j = 0; j < 5; j++)
				{
					datosProductos[i,j] = temp2[j];
				}
			}
			StartCoroutine (cargarImagenes ());
		}
	}

	IEnumerator cargarImagenes()
	{
		imagenesProductos =new Texture[datosProductos.GetLength(0)];
		for (int i = 0; i < datosProductos.GetLength(0); i++) {
			WWW www = new WWW(datosProductos[i,4]);
			yield return www;
			imagenesProductos [i] = www.texture;
		}
		datosCargados ();
	}

	void datosCargados(){
		CI.compradoycargados();
		botonLogin.SetActive (true);
		botonOmitir.SetActive (true);
		ponerDatos ();
	}

	void ponerDatos(){
		int alargar = 0;
		int tamaño = 372;
		if (datosProductos.GetLength (0) > 6) {
			alargar = Mathf.CeilToInt ((datosProductos.GetLength (0) - 6) / 2f);
			content.offsetMin = new Vector2 (0f, -1 * tamaño * alargar);
		}
		slots.transform.SetParent(content.transform);
		for(int i=0; i<datosProductos.GetLength(0);i++){
			slot [i].SetActive (true);
			slot [i].GetComponentInChildren<RawImage> ().texture = imagenesProductos [i];
			slot [i].GetComponentInChildren<Text> ().text = datosProductos [i, 0];
		}
	}

	public void ponerDetalle(int i){
		T.entrar (canvasDetalle);
		T.agregarPilaAtras (1);
		fotoDetalle.texture = imagenesProductos [i];
		nombreDetalle.text = datosProductos [i, 0];
		descripcionDetalle.text = datosProductos [i, 1];
		precioDetalle.text = datosProductos [i, 3];
	}
}
