﻿using UnityEngine;
using System.Collections;

public class splash : MonoBehaviour {

	public Animator anim;
    public GameObject logo;
    public GameObject fondo;

	// Use this for initialization
	void Start () {
		anim.SetBool ("mirror", true);
		anim.SetTrigger ("play");
	}

    public void destruirme() {
        Destroy(this.gameObject);
        Destroy(logo);
        Destroy(fondo);
    }
}
