﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using System.Net;
using System.IO;

public class ComprobarInternet : MonoBehaviour {

    public Image loading;
	public Text info;
	public CargarDatos CD;
    private bool cargando = true;
    private IEnumerator loadingIE1;


    void Start() {
		info.text = "";
		iniciarComprobacion();
    }

    public bool comprobador() {
        try {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead("http://www.google.com");
            stream.Close();
            return true;
        }
        catch {
            return false;
        }
    }

	public void iniciarComprobacion() {
        cargando = true;
        loadingIE1=loadingIE();
        StartCoroutine(loadingIE1);
		if (comprobador ()) {
			CD.iniciarCarga();
		}else{
			info.text="Comprueba tu conexión a internet y vuelve a intentarlo";
			StopCoroutine(loadingIE1);
        }
    }

	public void compradoycargados(){
		cargando = false;
		loading.gameObject.SetActive (false);
	}

    IEnumerator loadingIE() {
        float aux=0.01f;
        while (cargando) {
            if (loading.fillAmount <= 0) {
                aux = 0.01f;
                loading.fillClockwise = true;
            }else if (loading.fillAmount >= 1) { 
                aux = -0.01f;
                loading.fillClockwise = false;
            }
            loading.fillAmount = loading.fillAmount + aux;
            yield return new WaitForSeconds(0.01f);
        }
    }
}
